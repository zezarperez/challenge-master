import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from "./app.material-module";
import { HttpClientModule} from "@angular/common/http";

// Components
import { HomeComponent } from './components/home/home.component';
import { LabourCostComponent } from './components/reports/labour-cost/labour-cost.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LabourCostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
