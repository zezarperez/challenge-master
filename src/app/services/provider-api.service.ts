import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ProviderApiService {

  constructor(private http: HttpClient) { }

  // Set http options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  
  
  getProviders(): Observable<any[]> {
    return this.http.get<any[]>(environment.baseURL+'/application/labourstats');
  } 

}
