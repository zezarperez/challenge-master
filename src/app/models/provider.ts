export interface Provider {
    providerId: number;
    name: string;
    workerCount: number;
    complianceScore: number;
    grossPayTotal: number;
    payrollAdminTotal: number;
    labourCostTotal: number;
    workForcePercentage: number; 
    isDirectContrator: boolean;
}
