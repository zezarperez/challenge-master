import { Component, OnInit, ViewChild } from '@angular/core';
import { Provider } from "../../../models/provider";
import { ProviderApiService } from "../../../services/provider-api.service";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from '@angular/material/sort';
import * as _ from 'underscore';

@Component({
  selector: 'app-labour-cost',
  templateUrl: './labour-cost.component.html',
  styleUrls: ['./labour-cost.component.css']
})
export class LabourCostComponent implements OnInit {

	// Variables
	providers: Provider[] = [];
	totalData: any = {};
	sortData:  any = {};
	isLoadingData : boolean = false;
	isErrorData : boolean = false;

	// Create datasource for mat-table
	public displayedColumns = [ 'name', 'workerCount', 'complianceScore', 'grossPayTotal', 'payrollAdminTotal', 'labourCostTotal', 'workForcePercentage'];
	public dataSource = new MatTableDataSource<Provider>();
	@ViewChild(MatSort) sort: MatSort;

  constructor(private providerApiService: ProviderApiService) { }

  	ngOnInit() {
		this.getProvidersData();
		this.sortProviderTable();
	}

	ngAfterViewInit() {
		this.sortProviderTable();
	}

	// Custom sort function
	sortProviderTable(){
		this.dataSource.sort = this.sort;
		this.dataSource.sortData = (data: any[], sort: MatSort) => {
			var array: any[] = [];
			this.sortData = {'name': sort.active};
			if(sort.active == 'name'){
				var directContratorsArray = _.where(data, {isDirectContrator: true});
				var providersArray = _.where(data, {isDirectContrator: false});
				var filteredArray = _.sortBy(providersArray, function (i) { return i.name.toLowerCase(); });
				
				if(sort.direction == 'asc') filteredArray.reverse();
				array = directContratorsArray.concat(filteredArray);
			}
			else{
				array = _.sortBy(data, sort.active);
				if(sort.direction == 'desc') array.reverse()
			}
			return array
		}
	}

	// Function to fill datasource table
	getProvidersData(){
		this.providers = [];
		this.isLoadingData = true;
		this.isErrorData = false;

		this.providerApiService.getProviders().subscribe({
			complete: () => {
				this.isLoadingData = false;                
			},
			error: (err) => {
				this.isErrorData = true;
			}, 
			next: (res) => {
				this.formatProvidersData(res);
				this.dataSource.data = this.providers; 
			}
		});
	}

	// Function to transform providers array and perform operations
	formatProvidersData(data: any[]){

		// Select first element of array
		var providerObject = data[0];

		// Get data
		var providerList = providerObject.providers;
		var directContratorsList = providerObject.directContractors;
		this.totalData = providerObject.total[0];
		this.totalData.labourCostTotal = this.totalData.labourCostTotal / 100;
		this.totalData.grossPayTotal = this.totalData.grossPayTotal / 100;
		this.totalData.complianceTotal = this.totalData.complianceStats ? this.totalData.complianceStats.Total : 0,

		// Get list of providers
		providerList.forEach((element: any, index: number) => { 
			var workForceTotal = (element.workerCount / this.totalData.workerCount) * 100;
			var labourCost = element.labourCostTotal / 100;
			var grossPay = element.grossPayTotal / 100;
			this.providers.push({
				providerId: element.providerId,
				name: element.name,
				workerCount: element.workerCount,
				complianceScore: element.complianceStats ? element.complianceStats.Total : 0,
				grossPayTotal: grossPay,
				payrollAdminTotal: element.payrollAdminTotal,
				labourCostTotal: labourCost,
				workForcePercentage: workForceTotal,
				isDirectContrator: false
			});
		});

		// Get list of direct contrators
		directContratorsList.forEach((element: any, index: number) => { 
			var workForceTotal = (element.workerCount / this.totalData.workerCount) * 100;
			var labourCost = element.labourCostTotal / 100;
			var grossPay = element.grossPayTotal / 100;
			this.providers.push({
				providerId: element.providerId,
				name: element.name,
				workerCount: element.workerCount,
				complianceScore: element.complianceStats ? element.complianceStats.Total : 0,
				grossPayTotal: grossPay,
				payrollAdminTotal: element.payrollAdminTotal,
				labourCostTotal: labourCost,
				workForcePercentage: workForceTotal,
				isDirectContrator: true
			});
		});
	};

}
