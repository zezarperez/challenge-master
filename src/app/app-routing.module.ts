import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LabourCostComponent } from './components/reports/labour-cost/labour-cost.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent }, 
  { path: 'reports/labour-cost', component: LabourCostComponent },
  { path: '', pathMatch: 'full', redirectTo: '/reports/labour-cost' }, 
  { path: '**', pathMatch: 'full', redirectTo: '/reports/labour-cost' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }